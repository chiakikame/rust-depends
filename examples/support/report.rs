use rust_depends::chiakikame::depends::data::*;
use rust_depends::chiakikame::depends::data::import_table::*;
use rust_depends::chiakikame::depends::data::export_table::*;

pub trait Report {
    fn report(&self);
}

impl Report for Executable {
    fn report(&self) {
        match self.get_executable_format() {
            ExecutableFormat::PE => println!("Format: PE file"),
            ExecutableFormat::ELF => println!("Format: ELF file"),
        }

        println!("Import table");
        self.get_import_table().report();
        println!("End import table");
        println!("Export Table");
        self.get_export_table().report();
        println!("End export Table");
    }
}

impl<'a, T> Report for Option<&'a T>
where
    T: Report,
{
    fn report(&self) {
        match self {
            &Some(ref info) => info.report(),
            &None => println!("(None)"),
        }
    }
}

impl Report for ImportTable {
    fn report(&self) {
        for lib in self.get_libraries().iter() {
            lib.report();
        }

        for function in self.get_functions().iter() {
            function.report();
        }
    }
}

impl Report for ExportTable {
    fn report(&self) {
        println!("Ordinal\tName");
        for func in self.get_functions() {
            func.report();
        }
    }
}

impl Report for ImportLibrary {
    fn report(&self) {
        println!("\tLibrary {}", self.get_library_name());
    }
}

impl Report for ImportFunction {
    fn report(&self) {
        let identity_string = match self.get_function_identity() {
            &ImportFunctionIdentity::Name(ref n) => format!("{}", n),
            &ImportFunctionIdentity::Ordinal(n) => format!("#{}", n),
        };

        match self.get_source_library_name() {
            Some(name) => println!("\tFunction {1}::{0}", identity_string, name),
            None => println!("\tFunction {}", identity_string),
        };
    }
}

impl Report for ExportFunction {
    fn report(&self) {
        if self.get_name().is_some() {
            println!("\t{}\t{}", self.get_ordinal(), self.get_name().unwrap());
        } else {
            println!("\t{}", self.get_ordinal());
        }
    }
}
