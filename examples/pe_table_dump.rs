// Demo & test only
// DO NOT USE IT IN PRODUCTION

extern crate rust_depends;

mod support;

use std::env::args;
use std::fs::File;
use std::path::Path;

use rust_depends::chiakikame::depends::pe::*;
use rust_depends::chiakikame::depends::data::*;

use support::Report;

fn main() {
    let pe_path_str = args().nth(1).expect("No argument found");
    let pe_path: &Path = Path::new(&pe_path_str);

    let mut file = File::open(pe_path).expect("Cannot open file");

    let mut parser = PEParser::new();
    let executable: Executable = parser.parse_executable(&mut file).expect("parsing error");

    executable.report();
}
