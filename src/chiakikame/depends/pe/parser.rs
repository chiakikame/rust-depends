use std::io::{Read, Result, Seek, SeekFrom, Error, ErrorKind};
use byteorder::{ReadBytesExt, LittleEndian};
use chiakikame::depends::data::{Executable, ExecutableFormat};
use chiakikame::depends::data::import_table::*;
use chiakikame::depends::data::export_table::*;
use chiakikame::depends::data::parser::ExecutableParser;
use chiakikame::depends::data::parser::error::ParsingError;

pub struct PEParser {}

impl PEParser {
    pub fn new() -> Self {
        PEParser {}
    }
}

impl<Src: Read + Seek> ExecutableParser<Src> for PEParser {
    fn parse_executable(&mut self, r: &mut Src) -> Result<Executable> {
        parse_pe_image(r)
    }

    fn peek_signature(&mut self, r: &mut Src) -> Result<bool> {
        let current_pos = r.seek(SeekFrom::Current(0))?;
        let signature_result = read_signature(r);

        let ret = match signature_result {
            Ok(PESignature::PELE) => Ok(true),
            Ok(_) => Ok(false),
            Err(e) => Err(e),
        };

        r.seek(SeekFrom::Start(current_pos))?;
        ret
    }
}

fn parse_pe_image<Src>(r: &mut Src) -> Result<Executable>
where
    Src: ReadBytesExt + Seek,
{
    let start = r.seek(SeekFrom::Current(0))?;
    let signature = read_signature(r)?;

    if signature != PESignature::PELE {
        return make_error(
            r,
            ErrorKind::InvalidData,
            "PE signature is incorrect".to_owned(),
        );
    }

    let _ = r.read_u16::<LittleEndian>()?; // machine
    let num_of_sections = r.read_u16::<LittleEndian>()?;

    assert_ne!(num_of_sections, 0);
    // println!("{:x} {:x}", machine, num_of_sections);

    r.seek(SeekFrom::Current(16))?;
    // Now at optional header
    let optional_header_magic = r.read_u16::<LittleEndian>()?;
    let mode = match optional_header_magic {
        0x20b => PEMode::PE32Plus,
        0x10b => PEMode::PE32,
        _ => {
            return make_error(
                r,
                ErrorKind::InvalidData,
                format!(
                    "Optional header signature is incorrect ({:x})",
                    optional_header_magic
                ),
            );
        }
    };

    let jump_to_export_table = if mode.is64bit() { 110 } else { 94 };
    r.seek(SeekFrom::Current(jump_to_export_table))?;
    let export_table_rva = r.read_u32::<LittleEndian>()?;
    let jump_to_import_table = 4;
    r.seek(SeekFrom::Current(jump_to_import_table))?;
    let import_table_rva = r.read_u32::<LittleEndian>()?;

    let jump_to_section_tables = 116;
    r.seek(SeekFrom::Current(jump_to_section_tables))?;

    // Section tables
    let section_tables = (0..num_of_sections)
        .map(|_| SectionTable::from_read(r))
        .fold(ResultFolder::new(), ResultFolder::consume)
        .collect()?;

    let export_table_info = section_tables
        .iter()
        .filter(|section| {
            is_designated_table(section, ".edata", export_table_rva)
        })
        .nth(0);

    let import_table_info = section_tables
        .iter()
        .filter(|section| {
            is_designated_table(section, ".idata", import_table_rva)
        })
        .nth(0);

    let import_table = read_import_table(r, start, import_table_info, mode)?;
    let export_table = read_export_table(r, start, export_table_info, mode)?;

    Ok(Executable::new(
        import_table,
        export_table,
        ExecutableFormat::PE,
    ))
}

fn read_signature<Src>(r: &mut Src) -> Result<PESignature>
where
    Src: ReadBytesExt + Seek,
{
    let start = r.seek(SeekFrom::Current(0))?;

    // PE (actual PE image heading)
    r.seek(SeekFrom::Current(0x3C))?;
    let pos_of_pe_header = r.read_u32::<LittleEndian>()?;
    r.seek(SeekFrom::Start(start + pos_of_pe_header as u64))?;

    let mut signature = [0 as u8; 4];
    r.read(&mut signature)?;

    if signature[0] == 'P' as u8 && signature[1] == 'E' as u8 && signature[2] == 0 &&
        signature[3] == 0
    {
        Ok(PESignature::PELE)
    } else if signature[3] == 'P' as u8 && signature[2] == 'E' as u8 && signature[1] == 0 &&
               signature[0] == 0
    {
        // It's said if the signature looks like this,
        // the rest of the file is in big endian
        //
        // However, it's not documented in Microsoft's documentation,
        // besides, according to most of the documents about PE on the Internet,
        // it's assumed that PE files are in little endian format
        Ok(PESignature::PEBE)
    } else {
        Ok(PESignature::BADPE)
    }
}

fn read_export_table<Src>(
    r: &mut Src,
    pe_start: u64,
    export_table_info: Option<&SectionTable>,
    _: PEMode,
) -> Result<Option<ExportTable>>
where
    Src: Seek + ReadBytesExt,
{
    if export_table_info.is_none() {
        Ok(None)
    } else {
        let export_table_info = export_table_info.unwrap();
        let virtual_addr_of_section = export_table_info.virtual_address;
        let actual_pos_of_section = export_table_info.pos_relative_to_pe_start as u64 + pe_start;

        let to_actual_pos =
            |rva: u32| rva as u64 + actual_pos_of_section - virtual_addr_of_section as u64;

        r.seek(SeekFrom::Start(actual_pos_of_section))?;

        r.seek(SeekFrom::Current(12))?; // skip to name

        let name_pos = to_actual_pos(r.read_u32::<LittleEndian>()?);
        let ordinal_base = r.read_u32::<LittleEndian>()?;
        let _ = r.read_u32::<LittleEndian>()?; // address table entries
        let num_of_items = r.read_u32::<LittleEndian>()?;
        let _ = r.read_u32::<LittleEndian>()?; // address table rva
        let name_list_pos = to_actual_pos(r.read_u32::<LittleEndian>()?);
        let ordinal_list_pos = to_actual_pos(r.read_u32::<LittleEndian>()?);

        let name = read_ascii(r, name_pos)?;

        r.seek(SeekFrom::Start(pe_start + name_list_pos))?;
        let function_names = (0..num_of_items)
            .map(|_| r.read_u32::<LittleEndian>())
            .fold(ResultFolder::new(), ResultFolder::consume)
            .collect()?
            .into_iter()
            .map(|rva| if rva == 0 {
                Ok(None)
            } else {
                read_ascii(r, to_actual_pos(rva)).map(Some)
            })
            .fold(ResultFolder::new(), ResultFolder::consume)
            .collect()?;

        r.seek(SeekFrom::Start(ordinal_list_pos))?;
        let ordinals = (0..num_of_items)
            .map(|_| {
                r.read_u16::<LittleEndian>().map(
                    |v| v as u32 + ordinal_base,
                )
            })
            .fold(ResultFolder::new(), ResultFolder::consume)
            .collect()?;

        let functions = function_names
            .into_iter()
            .zip(ordinals.into_iter())
            .map(|(name_or_null, ordinal)| if let Some(name) = name_or_null {
                ExportFunction::with_name(ordinal, name)
            } else {
                ExportFunction::new(ordinal)
            })
            .collect::<Vec<ExportFunction>>();

        Ok(Some(ExportTable::with_functions(name, functions)))
    }
}

fn read_import_table<Src>(
    r: &mut Src,
    pe_start: u64,
    import_table_info: Option<&SectionTable>,
    mode: PEMode,
) -> Result<Option<ImportTable>>
where
    Src: Seek + ReadBytesExt,
{
    // first layer is reading import directory
    // each import directory corresponding to an actual import library

    if import_table_info.is_none() {
        Ok(None)
    } else {
        let import_table_info = import_table_info.unwrap();
        let virtual_addr_of_section = import_table_info.virtual_address;
        let actual_pos_of_section = import_table_info.pos_relative_to_pe_start as u64 + pe_start;

        let to_actual_pos =
            |rva: u32| rva as u64 + actual_pos_of_section - virtual_addr_of_section as u64;

        let mut libraries: Vec<ImportLibrary> = vec![];

        r.seek(SeekFrom::Start(actual_pos_of_section as u64))?;

        let mut functions: Vec<ImportFunction> = vec![];
        loop {
            let rva_of_func_table = r.read_u32::<LittleEndian>()?;

            if rva_of_func_table == 0 {
                break;
            } else {
                let actual_pos_of_func_table = to_actual_pos(rva_of_func_table);
                let _ = r.read_u32::<LittleEndian>()?; // timestamp
                let _ = r.read_u32::<LittleEndian>()?; // forward chain
                let name_pos = to_actual_pos(r.read_u32::<LittleEndian>()?);
                let _ = r.read_u32::<LittleEndian>()?; // import address table RVA

                let current_pos = r.seek(SeekFrom::Current(0))?;

                let name = read_ascii(r, name_pos)?;
                r.seek(SeekFrom::Start(current_pos))?;

                let functions_of_library =
                    read_import_lookup_table(r, mode, &name, rva_of_func_table, actual_pos_of_func_table)?;

                functions.extend(functions_of_library);

                libraries.push(ImportLibrary::new(name));

                r.seek(SeekFrom::Start(current_pos))?;
            }
        }

        Ok(Some(ImportTable::with_libraries_and_functions(libraries, functions)))
    }
}

fn read_import_lookup_table<Src>(
    r: &mut Src,
    mode: PEMode,
    library_name: &str,
    rva_of_func_table: u32,
    actual_pos_of_func_table: u64,
) -> Result<Vec<ImportFunction>>
where
    Src: Seek + ReadBytesExt,
{
    r.seek(SeekFrom::Start(actual_pos_of_func_table))?;

    let mut container: Vec<ImportFunction> = vec![];

    loop {
        let entry = if mode.is64bit() {
            r.read_u64::<LittleEndian>()?
        } else {
            r.read_u32::<LittleEndian>()? as u64
        };

        if entry == 0 {
            break;
        } else {
            let use_ordinal = if mode.is64bit() {
                entry & 0x8000000000000000 != 0
            } else {
                entry & 0x80000000 != 0
            };

            if use_ordinal {
                container.push(ImportFunction::with_library_name(library_name.to_owned(), ImportFunctionIdentity::Ordinal(entry as u16)));
            } else {
                // +2 for skipping hint field
                let name_pos = (entry & 0x8FFFFFFF) + actual_pos_of_func_table -
                    rva_of_func_table as u64 + 2;
                let current_pos = r.seek(SeekFrom::Current(0))?;
                container.push(ImportFunction::with_library_name(library_name.to_owned(), ImportFunctionIdentity::Name(read_ascii(r, name_pos)?)));
                r.seek(SeekFrom::Start(current_pos))?;
            }
        }
    }

    Ok(container)
}

fn read_ascii<Src>(r: &mut Src, actual_pos_of_string: u64) -> Result<String>
where
    Src: Seek + ReadBytesExt,
{
    let mut s: String = String::new();
    r.seek(SeekFrom::Start(actual_pos_of_string))?;
    loop {
        let ch = r.read_u8()? as char;
        if ch == '\0' {
            break;
        } else {
            s.push(ch);
        }
    }
    Ok(s)
}

fn make_error<Src>(r: &mut Src, kind: ErrorKind, message: String) -> Result<Executable>
where
    Src: Seek,
{
    let pos = r.seek(SeekFrom::Current(0))?;
    Err(Error::new(kind, ParsingError::new(pos, message)))
}

fn is_designated_table(section: &SectionTable, name: &str, table_rva: u32) -> bool {
    section.name == name ||
        (section.virtual_address <= table_rva &&
             section.virtual_address + section.virtual_size > table_rva)
}

#[derive(PartialEq)]
enum PESignature {
    PELE,
    PEBE,
    BADPE,
}

#[derive(Copy, Clone, Debug)]
enum PEMode {
    PE32Plus,
    PE32,
}

impl PEMode {
    fn is64bit(&self) -> bool {
        match self {
            &PEMode::PE32Plus => true,
            _ => false,
        }
    }
}

#[derive(Debug)]
struct SectionTable {
    name: String,
    virtual_size: u32,
    virtual_address: u32,
    pos_relative_to_pe_start: u32,
}

impl SectionTable {
    fn from_read<Src>(r: &mut Src) -> Result<Self>
    where
        Src: ReadBytesExt + Seek,
    {
        let mut name = [0 as u8; 8];
        r.read(&mut name)?;

        let virtual_size = r.read_u32::<LittleEndian>()?;
        let virtual_address = r.read_u32::<LittleEndian>()?;
        r.read_u32::<LittleEndian>()?; // actual size
        let actual_pos = r.read_u32::<LittleEndian>()?;
        r.seek(SeekFrom::Current(16))?; // consume unused section table items

        Ok(SectionTable {
            name: String::from_utf8_lossy(&name).into_owned(),
            virtual_size,
            virtual_address,
            pos_relative_to_pe_start: actual_pos,
        })
    }
}

enum ResultFolder<T> {
    Init,
    Ok(Vec<T>),
    Err(Error),
}

impl<T> ResultFolder<T> {
    fn new() -> Self {
        ResultFolder::Init
    }

    fn consume(self, r: Result<T>) -> Self {
        match self {
            ResultFolder::Err(e) => ResultFolder::Err(e),
            ResultFolder::Init => {
                match r {
                    Ok(v) => ResultFolder::Ok(vec![v]),
                    Err(e) => ResultFolder::Err(e),
                }
            }
            ResultFolder::Ok(mut vec) => {
                match r {
                    Ok(v) => {
                        vec.push(v);
                        ResultFolder::Ok(vec)
                    }
                    Err(e) => ResultFolder::Err(e),
                }
            }
        }
    }

    fn collect(self) -> Result<Vec<T>> {
        match self {
            ResultFolder::Init => Ok(vec![]),
            ResultFolder::Ok(vec) => Ok(vec),
            ResultFolder::Err(e) => Err(e),
        }
    }
}
