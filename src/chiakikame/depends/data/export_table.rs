#[derive(Debug)]
pub struct ExportTable {
    functions: Vec<ExportFunction>,
    library_name: String,
}

#[derive(Debug)]
pub struct ExportFunction {
    name: Option<String>,
    ordinal: u32,
}

impl ExportTable {
    pub fn with_functions(name: String, funcs: Vec<ExportFunction>) -> Self {
        ExportTable {
            functions: funcs,
            library_name: name,
        }
    }

    pub fn get_functions(&self) -> &[ExportFunction] {
        &self.functions
    }

    pub fn get_library_name(&self) -> &str {
        &self.library_name
    }
}

impl ExportFunction {
    pub fn new(ordinal: u32) -> Self {
        ExportFunction {
            name: None,
            ordinal,
        }
    }

    pub fn with_name(ordinal: u32, name: String) -> Self {
        ExportFunction {
            name: Some(name),
            ordinal,
        }
    }

    pub fn get_name(&self) -> Option<&str> {
        self.name.as_ref().map(|s| s.as_ref())
    }

    pub fn get_ordinal(&self) -> u32 {
        self.ordinal
    }
}
