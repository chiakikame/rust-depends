use std::path::{Path, PathBuf};

pub trait DependencySolver {
    fn load_main_executable_from_disk(&mut self, path: &Path) -> Result<(), String>;
    fn solve_dependency(&mut self) -> Result<(), String>;
    fn get_dependency_report(&self) -> &DependencyReport;
}

pub struct DependencyReport {
    executable_full_path: PathBuf,
    satisfied_dependency: Vec<DependencyInfo>,
    missing_dependency: Vec<DependencyInfo>,
}

impl DependencyReport {
    pub fn get_executable_full_path(&self) -> &Path {
        &self.executable_full_path
    }

    pub fn get_satisfied_dependencies(&self) -> &[DependencyInfo] {
        &self.satisfied_dependency
    }

    pub fn get_missing_dependencies(&self) -> &[DependencyInfo] {
        &self.missing_dependency
    }
}

pub struct DependencyInfo {
    library_name: String,
    function_name: String,
}

impl DependencyInfo {
    pub fn get_library_name(&self) -> &str {
        &self.library_name
    }

    pub fn get_function_name(&self) -> &str {
        &self.function_name
    }
}
