#[derive(Debug)]
pub struct ImportTable {
    referenced_libraries: Vec<ImportLibrary>,
    required_functions: Vec<ImportFunction>,
}

impl ImportTable {
    pub fn with_libraries_and_functions(libs: Vec<ImportLibrary>, funcs: Vec<ImportFunction>) -> Self {
        ImportTable { referenced_libraries: libs, required_functions: funcs }
    }

    pub fn get_libraries(&self) -> &[ImportLibrary] {
        &self.referenced_libraries
    }

    pub fn get_functions(&self) -> &[ImportFunction] {
        &self.required_functions
    }
}

#[derive(Debug)]
pub struct ImportLibrary {
    library_name: String,
}

impl ImportLibrary {
    pub fn new(name: String) -> Self {
        ImportLibrary {
            library_name: name,
        }
    }

    pub fn get_library_name(&self) -> &str {
        &self.library_name
    }
}

#[derive(Debug)]
pub struct ImportFunction {
    source_library: Option<String>,
    identity: ImportFunctionIdentity,
}

impl ImportFunction {
    pub fn with_library_name(source_library: String, function: ImportFunctionIdentity) -> Self {
        ImportFunction {
            source_library: Some(source_library),
            identity: function,
        }
    }

    pub fn function_identity_only(function: ImportFunctionIdentity) -> Self {
        ImportFunction {
            source_library: None,
            identity: function,
        }
    }

    pub fn get_source_library_name(&self) -> Option<&str> {
        self.source_library.as_ref().map(|s| s.as_ref())
    }

    pub fn get_function_identity(&self) -> &ImportFunctionIdentity {
        &self.identity
    }
}

#[derive(Debug)]
pub enum ImportFunctionIdentity {
    Name(String),
    Ordinal(u16),
}
