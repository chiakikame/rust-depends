pub mod import_table;
pub mod export_table;
pub mod parser;
pub mod dependency_solver;

pub use self::import_table::ImportTable;
pub use self::export_table::ExportTable;
pub use self::parser::ExecutableParser;

#[derive(Debug)]
pub struct Executable {
    import_table: Option<ImportTable>,
    export_table: Option<ExportTable>,
    executable_format: ExecutableFormat,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum ExecutableFormat {
    PE,
    ELF,
}

impl Executable {
    pub fn new(
        import_table: Option<ImportTable>,
        export_table: Option<ExportTable>,
        executable_format: ExecutableFormat,
    ) -> Self {
        Executable {
            import_table,
            export_table,
            executable_format,
        }
    }

    pub fn get_export_table(&self) -> Option<&ExportTable> {
        self.export_table.as_ref()
    }

    pub fn get_import_table(&self) -> Option<&ImportTable> {
        self.import_table.as_ref()
    }

    pub fn get_executable_format(&self) -> ExecutableFormat {
        self.executable_format
    }
}
