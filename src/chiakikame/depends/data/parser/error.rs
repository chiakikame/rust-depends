use std::error::Error;
use std::fmt::{Display, Formatter};
use std::fmt;
use std::io;

#[derive(Debug)]
pub struct ParsingError {
    current_pos: u64,
    kind: String,
    cause: Option<ParsingErrorCauses>,
    description: String,
}

#[derive(Debug)]
enum ParsingErrorCauses {
    IOError(io::Error),
}

impl ParsingError {
    pub fn new(current_pos: u64, kind: String) -> ParsingError {
        let description = make_description(&kind, current_pos);
        ParsingError {
            current_pos,
            kind,
            cause: None,
            description,
        }
    }

    pub fn new_with_io_error(current_pos: u64, kind: String, cause: io::Error) -> ParsingError {
        ParsingError::new_with_cause(current_pos, kind, ParsingErrorCauses::IOError(cause))
    }

    fn new_with_cause(current_pos: u64, kind: String, cause: ParsingErrorCauses) -> ParsingError {
        let description = make_description(&kind, current_pos);
        ParsingError {
            current_pos,
            kind,
            cause: Some(cause),
            description,
        }
    }
}

fn make_description(kind: &str, current_pos: u64) -> String {
    format!("Error {} at position {}", kind, current_pos)
}

impl Display for ParsingError {
    fn fmt(&self, f: &mut Formatter) -> Result<(), fmt::Error> {
        f.write_str(self.description())?;
        Ok(())
    }
}

impl Error for ParsingError {
    fn description(&self) -> &str {
        &self.description
    }

    fn cause(&self) -> Option<&Error> {
        self.cause.as_ref().map(|c| match c {
            &ParsingErrorCauses::IOError(ref e) => e as &Error,
        })
    }
}
