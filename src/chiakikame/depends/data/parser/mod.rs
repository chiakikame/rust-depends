use std::io::{Read, Result, Seek};
use chiakikame::depends::data::Executable;

pub mod error;

pub trait ExecutableParser<Src: Read + Seek> {
    fn parse_executable(&mut self, r: &mut Src) -> Result<Executable>;
    fn peek_signature(&mut self, r: &mut Src) -> Result<bool>;
}
