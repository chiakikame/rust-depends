# Executable dependency exploration toolkit

This is the repository of executable dependency exploration toolkit (`rust-depends`).

This project is still in early stage.

## Feature

* Loads static (non delay-loaded) import and export information of an
 executable or a library
 
  It's possible to load executable from a `Read` stream.

## Supported format

* Windows PE file (`*.exe`, `*.dll`)

## TODO

* Add support for ELF format

* Able to determine if dependency of given executable / library is satisfied
  when `PATH` / set of library are assigned.